import React from 'react';
import AppBar from '@material-ui/core/es/AppBar/AppBar';
import Toolbar from '@material-ui/core/es/Toolbar/Toolbar';
import MoreVerticalIcon from '@material-ui/icons/MoreVert';
import debug from 'debug';
import MoveOrRotateIcon from '../icons/moveOrRotare';
import EditIcon from '../icons/edit';
import MarkerIcon from '../icons/marker';
import LineIcon from '../icons/line';
import MODES from '../editorModel/modes';
import OBJECT_TYPES from '../object/types';
import ModesHelper from '../editorModel/modesHelper';
import EDITOR_MODEL_EVENT_TYPES from '../editorModel/eventTypes';
import EDITOR_EVENT_TYPES from './eventTypes';
import ToolbarIcon from './toolbarIcon';
import { StyledToolbar } from './topToolbar/styled';

const log = debug('top-menu');

const SPACER = 'spacer';

export default class TopToolbar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      icons: this.getIcons(),
    };
  }

  getIcons() {
    return [
      {
        title: 'Move or rotate',
        icon: <MoveOrRotateIcon />,
        color: ModesHelper.isMoveOrRotateMode(this.props.editorModel)
          ? 'red'
          : 'black',
        action: this.setMoveOrRotateMode,
      },
      {
        title: 'Edit',
        icon: <EditIcon />,
        color: ModesHelper.isEditMode(this.props.editorModel) ? 'red' : 'black',
        action: this.setEditMode,
      },
      {
        title: 'Draw marker',
        icon: <MarkerIcon />,
        color: ModesHelper.isDrawingMarker(this.props.editorModel)
          ? 'red'
          : 'black',
        action: this.setDrawMarkerMode,
      },
      {
        title: 'Draw line',
        icon: <LineIcon />,
        color: ModesHelper.isDrawingLine(this.props.editorModel)
          ? 'red'
          : 'black',
        action: this.setDrawLineMode,
      },
      SPACER,
      {
        title: 'Layers',
        icon: <MoreVerticalIcon />,
        color: 'primary',
        action: this.toggleRightDrawer,
      },
    ];
  }

  setDrawLineMode = () => {
    this.props.editorModel.setMode(MODES.DRAW_MODE, {
      drawModeType: OBJECT_TYPES.LINE,
    });
  };

  setDrawMarkerMode = () => {
    this.props.editorModel.setMode(MODES.DRAW_MODE, {
      drawModeType: OBJECT_TYPES.MARKER,
    });
  };

  setMoveOrRotateMode = () => {
    this.props.editorModel.setMode(MODES.MOVE_OR_ROTATE_MODE);
  };

  setEditMode = () => {
    this.props.editorModel.setMode(MODES.EDIT_MODE);
  };

  onModeChange = (newMode, newModeOptions) => {
    log(`Mode changed to ${newMode} with options: `, newModeOptions);
    this.setState({
      icons: this.getIcons(),
    });
  };

  componentDidMount() {
    this.props.editorModel.on(
      EDITOR_MODEL_EVENT_TYPES.MODE.CHANGE,
      this.onModeChange,
    );
  }

  toggleRightDrawer = () => {
    this.props.editorModel.showRightDrawer(
      !this.props.editorModel.rightDrawer.opened,
    );
  };

  render() {
    const { theme } = this.props;
    return (
      <AppBar position="static" color="default">
        <StyledToolbar variant="dense">
          {this.state.icons.map((item, idx) => {
            if (item === SPACER) {
              return <div key={idx} style={{ flexGrow: 1 }} />;
            }
            return <ToolbarIcon key={idx} {...item} />;
          })}
        </StyledToolbar>
      </AppBar>
    );
  }
}

import React from 'react';
import { withTheme } from '@material-ui/core/styles/index';
import { StyledIcon } from './topToolbar/styled';

export default withTheme()(props => {
  const { theme } = props;
  const newProps = {
    color: props.color,
  };
  return (
    <StyledIcon
      onClick={props.action}
      title={props.title}
      theme={theme}
      aria-label={props.title}>
      {React.cloneElement(props.icon, newProps)}
    </StyledIcon>
  );
});

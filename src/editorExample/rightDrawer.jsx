import React from 'react';
import { withTheme } from '@material-ui/core/styles';
import styled from 'styled-components';
import Grid from '@material-ui/core/es/Grid/Grid';
import Button from '@material-ui/core/es/Button/Button';
import PropTypes from 'prop-types';
import Divider from '@material-ui/core/es/Divider/Divider';
import CloseIcon from '@material-ui/icons/Close';
import Toolbar from '../../node_modules/@material-ui/core/Toolbar/Toolbar';
import EDITOR_MODEL_EVENT_TYPES from '../editorModel/eventTypes';
import EDITOR_EVENT_TYPES from './eventTypes';
import eventBus from '../eventBus';
import LayersListWidget from './rightDrawer/layersListWidget';
import { EditorModel } from '../editorModel';
import ToolbarIcon from './toolbarIcon';
import { StyledToolbar } from './topToolbar/styled';

const DRAWER_WIDTH = '16em';

const Drawer = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  transition: transform 0.15s ease-out;
  transform: translate3d(${props => (props.open ? 0 : DRAWER_WIDTH)}, 0, 0);
  z-index: 1500;
  height: calc(${props => props.height}px);
  width: calc(${DRAWER_WIDTH} - 0.5em - 0.5em);
  background-color: white;
  padding: 0;
`;

class RightDrawer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      opened: false,
      layers: this.props.editorModel.layers,
      selectedLayer: this.props.editorModel.selectlayer,
    };
  }

  onLayersChanged = newLayers => {
    this.setState({
      layers: this.props.editorModel.layers,
    });
  };

  onLayersSelectionChanged = selectedlayer => {
    this.setState({
      selectedLayer: selectedlayer,
    });
  };

  onDrawerVisibilitychnaged = opened => {
    this.setState({
      opened,
    });
  };

  close = () => {
    this.props.editorModel.showRightDrawer(false);
  };

  componentDidMount() {
    this.props.editorModel.on(
      EDITOR_MODEL_EVENT_TYPES.RIGHT_DRAWER.VISIBILITY_CHANGED,
      this.onDrawerVisibilitychnaged,
    );
    this.props.editorModel.on(
      EDITOR_MODEL_EVENT_TYPES.LAYERS.CHANGED,
      this.onLayersChanged,
    );
    this.props.editorModel.on(
      EDITOR_MODEL_EVENT_TYPES.LAYERS.SELECTION_CHANGED,
      this.onLayersSelectionChanged,
    );
  }

  toggleLayerVisibility(evt, layer) {
    evt.stopPropagation();
  }

  render() {
    // const {theme} = this.props;
    return (
      <Drawer open={this.state.opened} height={this.props.height}>
        <StyledToolbar variant="dense">
          <div style={{ flexGrow: 1 }} />
          <ToolbarIcon icon={<CloseIcon />} action={this.close} />
        </StyledToolbar>
        <LayersListWidget
          editorModel={this.props.editorModel}
          layers={this.state.layers}
          selectedLayer={this.state.selectedLayer}
        />
        <Divider />
      </Drawer>
    );
  }
}

export default withTheme()(RightDrawer);

RightDrawer.propTypes = {
  editorModel: PropTypes.instanceOf(EditorModel).isRequired,
};

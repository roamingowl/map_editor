import React from 'react';
import L from 'leaflet';
import LdrawDrag from 'leaflet-draw-drag';
import LeafletDrawTransform from 'leaflet-path-transform';
import leafletCss from 'leaflet/dist/leaflet.css';
import leafletDrawCss from 'leaflet-draw/dist/leaflet.draw.css';
import styled from 'styled-components';
import debug from 'debug';
import uuid from 'uuid';
import BaseLayer from './mapLayers/baseLayer';
import EDITOR_MODEL_EVENT_TYPES from '../editorModel/eventTypes';
import EDITOR_EVENT_TYPES from './eventTypes';
import MODES from '../editorModel/modes';
import TYPES from '../object/types';
import { DrawMarker, Marker, MarkerIcon } from './map/mapMarker';
import eventBus from '../eventBus';

const MapDiv = styled.div`
  height: ${props => props.height}px;
  width: 100%;
  top: 48px;
  left: 0;
  position: absolute;
  overflow: hidden;
`;

const log = debug('map-canvas');

export default class MapCanvas extends React.Component {
  constructor(props) {
    super(props);
    this.id = props.id || uuid();
    this.map = null;
    this.mapSettings = {
      zoomControl: false,
    };
    this.state = {
      zoom: this.props.editorModel.zoom,
    };
  }

  onMapZoomChanged = evt => {
    this.props.editorModel.setZoom(this.map.getZoom());
  };

  _onMarkerCreated = darwingObject => {
    const newMarker = darwingObject.layer;
    const markerData = {
      name: 'Marker' + this.itemCounter,
      type: 'marker',
      id: uuid(),
      layerId: this.props.editorModel.selectedLayer.id,
      layer: this.props.editorModel.selectedLayer,
      lat: newMarker.getLatLng().lat,
      lng: newMarker.getLatLng().lng,
      marker: newMarker,
    };
    newMarker.data = markerData;
    // darwingObject.layer.once('click', (evt) => this.onMarkerClicked(evt, markerInstance));
    // markerInstance.layer.on('move', (evt) => {
    //     //TODO: if you want to multislect move - do it here!
    //     console.log('moved', evt);
    // });
    this.props.editorModel.selectedLayer.mapLayer.addLayer(newMarker);
    // if (!this.state.selectedLayer.markers) {
    //     this.state.selectedLayer.markers = [];
    // }
    // this.props.editorModel.selectedLayer.add(markerData);
    // this.itemCounter++;
    // this.setState({
    //     //markersCounter: this.state.markersCounter + 1,
    //     layers: this.state.layers,
    //     model:  this.layersToModel(this.state.layers)
    // });
  };

  onDrawingCreated = darwingObject => {
    if (darwingObject.layerType === 'marker') {
      return this._onMarkerCreated(darwingObject);
    }
    console.warn('Unknown drawing object created', darwingObject);
  };

  componentDidMount() {
    const baseLayer = BaseLayer.create({
      minZoom: this.props.editorModel.minZoom,
      maxZoom: this.props.editorModel.maxZoom,
    });
    this.map = L.map(this.id, this.mapSettings).setView(
      [37.75, -122.23],
      this.state.zoom,
    );
    L.tileLayer(baseLayer.url, baseLayer.options).addTo(this.map);
    this.map.on('zoomend', this.onMapZoomChanged);
    this.map.on('draw:created', this.onDrawingCreated);
    this.props.editorModel.on(
      EDITOR_MODEL_EVENT_TYPES.MODE.CHANGE,
      this.onEditorModeChange,
    );
    eventBus.on(
      EDITOR_EVENT_TYPES.LAYER.CREATE_AND_ADD,
      this.onCreateAndAddLayer,
    );
    // TODO: do not use setTimeout - find out a better place for this
    setTimeout(() => {
      if (this.props.editorModel.layers.length === 0) {
        const layer = this._createNewLayer();
        this.props.editorModel.selectLayer(layer);
      }
    }, 500);
  }

  _createNewLayer = () => {
    const drawLayer = new L.FeatureGroup();
    this.map.addLayer(drawLayer);
    return this.props.editorModel.createAndAddLayer(this.map, drawLayer);
  };

  onCreateAndAddLayer = () => {
    this._createNewLayer();
  };

  onEditorModeChange = (mode, modeOptions) => {
    log('map mode change');
    switch (true) {
      case mode === MODES.DRAW_MODE &&
        modeOptions.drawModeType === TYPES.MARKER:
        log('drawing marker');
        const marker = new DrawMarker(this.map, {
          icon: MarkerIcon,
        });
        marker.enable();
        break;
    }
  };

  render() {
    return <MapDiv height={this.props.height} id={this.id} />;
  }
}

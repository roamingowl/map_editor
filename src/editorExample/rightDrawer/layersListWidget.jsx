import React from 'react';
import ListItem from '@material-ui/core/es/ListItem/ListItem';
import IconButton from '@material-ui/core/es/IconButton/IconButton';
import List from '@material-ui/core/es/List/List';
import styled from 'styled-components';
import ListItemText from '@material-ui/core/es/ListItemText/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/es/ListItemSecondaryAction/ListItemSecondaryAction';
import TrashIcon from '@material-ui/icons/Delete';
import VisibleIcon from '@material-ui/icons/Visibility';
import ExpandIcon from '@material-ui/icons/ExpandMore';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/es/Grid/Grid';
import Button from '@material-ui/core/es/Button/Button';
import Toolbar from '../../../node_modules/@material-ui/core/Toolbar/Toolbar';
import { EditorModel } from '../../editorModel';
import EDITOR_EVENT_TYPES from '../eventTypes';
import eventBus from '../../eventBus';

// TODO: move to LayersWidget
const StyledIconButton = styled(IconButton)`
  width: 24px !important;
  height: 24px !important;
`;

const Widget = styled(Grid)`
  padding: 0.5em;
`;

const StyledList = styled(List)`
  max-height: 500px;
  overflow-y: auto;
`;

const StylesListItem = styled(ListItem)`
  min-width: 14em;
  padding-left: 0.5em !important;
  background-color: ${props =>
    props.selected ? '#e4e6ff' : '#ffffff'} !important;
`;

export default class LayersListWidget extends React.Component {
  selectlayer = layer => {
    this.props.editorModel.selectLayer(layer);
  };

  addLayer() {
    eventBus.emit(EDITOR_EVENT_TYPES.LAYER.CREATE_AND_ADD);
  }

  render() {
    return (
      <Widget container direction="column">
        <Grid item>
          <StyledList>
            {this.props.layers.map((layer, key) => (
              <StylesListItem
                key={key}
                selected={
                  this.props.selectedLayer &&
                  layer.id === this.props.selectedLayer.id
                }
                button
                dense
                onClick={() => {
                  this.selectlayer(layer);
                }}>
                <ListItemText>
                  <StyledIconButton
                    onClick={evt => this.toggleLayerVisibility(evt, layer)}
                    aria-label="Visibility">
                    <VisibleIcon />
                  </StyledIconButton>
                  {layer.meta.name}
                </ListItemText>
                <ListItemSecondaryAction>
                  <StyledIconButton aria-label="Delete">
                    <TrashIcon />
                  </StyledIconButton>
                  {layer.children.length > 0 ? (
                    <StyledIconButton aria-label="Expand">
                      <ExpandIcon />
                    </StyledIconButton>
                  ) : (
                    <div style={{ width: 24 }} />
                  )}
                </ListItemSecondaryAction>
              </StylesListItem>
            ))}
          </StyledList>
        </Grid>
        <Grid item>
          <Button onClick={this.addLayer} variant="contained" color="primary">
            Add layer
          </Button>
        </Grid>
      </Widget>
    );
  }
}

LayersListWidget.propTypes = {
  editorModel: PropTypes.instanceOf(EditorModel).isRequired,
};

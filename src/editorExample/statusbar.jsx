import React from 'react';
import { withTheme } from '@material-ui/core/styles/index';
import debug from 'debug';
import Grid from '@material-ui/core/es/Grid/Grid';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import EDITOR_MODEL_EVENT_TYPES from '../editorModel/eventTypes';
import MODES from '../editorModel/modes';
import { EditorModel } from '../editorModel';

const log = debug('statusBar');

const StatusbarItem = styled.div`
  margin-left: 8px;
  padding: 4px;
`;

const StyledGrid = styled(Grid)`
  background-color: ${props => props.theme.palette.grey['100']};
  width: 100%;
  height: 24px;
  font-size: 12px;
`;

class StatusBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      mode: this.getModeString(this.props.editorModel.mode),
      zoom: this.props.editorModel.zoom,
      selectedLayer: this.getLayerName(this.props.editorModel.selectedLayer),
    };
  }

  getModeString = mode => {
    if (mode === MODES.EDIT_MODE) {
      return 'ED';
    }
    if (mode === MODES.DRAW_MODE) {
      return 'DR';
    }
    if (mode === MODES.MOVE_OR_ROTATE_MODE) {
      return 'MR';
    }
    return 'UN';
  };

  getLayerName = layer => {
    if (!layer) {
      return 'none';
    }
    return layer.name;
  };

  onEditorModelChange = modeInfo => {
    this.setState({
      mode: this.getModeString(this.props.editorModel.mode),
      zoom: this.props.editorModel.zoom,
      selectedLayer: this.getLayerName(this.props.editorModel.selectedLayer),
    });
  };

  componentDidMount() {
    this.props.editorModel.on(
      EDITOR_MODEL_EVENT_TYPES.STATE.CHANGE,
      this.onEditorModelChange,
    );
  }

  render() {
    const { theme } = this.props;
    return (
      <StyledGrid theme={theme} container spacing={0} alignItems="center">
        <Grid item>
          <StatusbarItem>Mode: {this.state.mode}</StatusbarItem>
        </Grid>
        <Grid item>
          <StatusbarItem>Zoom: {this.state.zoom}</StatusbarItem>
        </Grid>
        <Grid item>
          <StatusbarItem>Layer: {this.state.selectedLayer}</StatusbarItem>
        </Grid>
      </StyledGrid>
    );
  }
}

export default withTheme()(StatusBar);

StatusBar.propTypes = {
  editorModel: PropTypes.instanceOf(EditorModel).isRequired,
};

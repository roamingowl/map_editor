import styled from 'styled-components';
import IconButton from '@material-ui/core/es/IconButton/IconButton';
import Toolbar from '@material-ui/core/es/Toolbar/Toolbar';

const StyledIcon = styled(IconButton)`
  margin: ${props => props.theme.spacing.unit}
    ${props => props.theme.spacing.unit} ${props => props.theme.spacing.unit} 0;
  padding: 0;
  width: ${props =>
    props.theme.mixins.toolbar.minHeight - props.theme.spacing.unit * 2}px;
  height: ${props =>
    props.theme.mixins.toolbar.minHeight - props.theme.spacing.unit * 2}px;
`;

const StyledToolbar = styled(Toolbar)`
  padding-left: 0 !important;
  padding-right: 0 !important;
`;

export { StyledIcon, StyledToolbar };

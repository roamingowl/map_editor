export default {
  STATE: {
    CHANGE: 'state_change',
  },
  MODE: {
    CHANGE: 'mode_change',
  },
  MAP: {
    ZOOM_CHANGE: 'map_zoom_change',
  },
  LAYERS: {
    CHANGED: 'layers_changed',
    SELECTION_CHANGED: 'layers_selection_changed',
  },
  MARKER: {
    CREATED: 'marker_created',
  },
  RIGHT_DRAWER: {
    VISIBILITY_CHANGED: 'right_drawer_visibility_changed',
  },
};

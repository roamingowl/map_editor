export default {
  MOVE_OR_ROTATE_MODE: 'move_or_rotate_mode',
  EDIT_MODE: 'edit_mode',
  DRAW_MODE: 'draw_mode',
};

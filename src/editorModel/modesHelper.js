import MODES from './modes';
import TYPES from '../object/types';

export default class ModesHelper {
  static isMoveOrRotateMode(editormodel) {
    return editormodel.mode === MODES.MOVE_OR_ROTATE_MODE;
  }

  static isEditMode(editormodel) {
    return editormodel.mode === MODES.EDIT_MODE;
  }

  static isDrawingMarker(editormodel) {
    return (
      editormodel.mode === MODES.DRAW_MODE &&
      editormodel.drawModeType === TYPES.MARKER
    );
  }

  static isDrawingLine(editormodel) {
    return (
      editormodel.mode === MODES.DRAW_MODE &&
      editormodel.drawModeType === TYPES.LINE
    );
  }
}

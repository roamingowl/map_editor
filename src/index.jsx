import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';
import styles from './index.css';

ReactDOM.render(<App />, document.getElementById('app'));

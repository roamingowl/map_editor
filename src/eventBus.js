import EventEmitter from './eventEmitter';

/**
 * @singleton
 */
export default new EventEmitter();

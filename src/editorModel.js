import uuid from 'uuid';
import MODES from './editorModel/modes';
import EventEmitter from './eventEmitter';
import EVENT_TYPES from './editorModel/eventTypes';
import Layer from './object/layer';

class EditorModel extends EventEmitter {
  constructor() {
    super();
    this._layerCounter = 1;
    this._markerCounter = 1;
    this.state = Object.freeze({
      layers: [],
      mode: MODES.MOVE_OR_ROTATE_MODE,
      drawModeType: null,
      selectedLayer: null,
      minZoom: 10,
      maxZoom: 25,
      zoom: 13,
      rightDrawer: {
        opened: false,
      },
    });
  }

  get rightDrawer() {
    return this.state.rightDrawer;
  }

  get minZoom() {
    return this.state.minZoom;
  }

  get maxZoom() {
    return this.state.maxZoom;
  }

  get zoom() {
    return this.state.zoom;
  }

  get mode() {
    return this.state.mode;
  }

  get layers() {
    return this.state.layers;
  }

  get drawModeType() {
    return this.state.drawModeType;
  }

  get selectedLayer() {
    return this.state.selectedLayer;
  }

  setZoom(zoom) {
    this.setState({
      zoom,
    });
    this.emit(EVENT_TYPES.MAP.ZOOM_CHANGE, zoom);
  }

  setState(newState) {
    this.state = Object.freeze(Object.assign({}, this.state, newState));
    this.emit(EVENT_TYPES.STATE.CHANGE, this.state);
  }

  createAndAddMarker(map, newMarker, targetLayer) {
    const markerData = {
      name: 'Marker' + this.itemCounter,
      type: 'marker',
      id: uuid(),
      map,
      layerId: this.props.editorModel.selectedLayer.id,
      layer: this.props.editorModel.selectedLayer,
      lat: newMarker.getLatLng().lat,
      lng: newMarker.getLatLng().lng,
      marker: newMarker,
    };
    targetLayer.add(markerData);
    this.emit(EVENT_TYPES.MARKER.CREATED, markerData, targetLayer);
    this.emit(EVENT_TYPES.LAYERS.CHANGED, this.layers);
  }

  // TODO: add target layer to support layers heirarchy
  createAndAddLayer(map, drawLayer) {
    const layer = new Layer({
      map,
      mapLayer: drawLayer,
      meta: {
        name: `Layer ${this._layerCounter}`,
      },
    });
    this._layerCounter++;
    const layers = this.state.layers;
    layers.push(layer);
    this.setState({
      layers,
    });
    this.emit(EVENT_TYPES.LAYERS.CHANGED, this.layers);
    return layer;
  }

  _addLayer(layer) {
    this.layers.push(layer);
  }

  selectLayer(layer) {
    for (let i = 0; i < this.layers.length; i++) {
      if (this.state.layers[i].id === layer.id) {
        this.setState({
          selectedLayer: layer,
        });
        this.emit(EVENT_TYPES.LAYERS.SELECTION_CHANGED, this.selectedLayer);
        return;
      }
    }
    throw new Error('Layer not found');
  }

  toJson() {
    return this.state.layers.map(layer => layer.toJson());
  }

  showRightDrawer(show) {
    this.setState({
      rightDrawer: Object.assign({}, this.state.rightDrawer, { opened: show }),
    });
    this.emit(EVENT_TYPES.RIGHT_DRAWER.VISIBILITY_CHANGED, show);
  }

  setMode(mode, modeOptions) {
    const newState = {
      mode,
    };
    if (mode === MODES.DRAW_MODE) {
      newState.drawModeType = modeOptions.drawModeType;
    }
    this.setState(newState);
    this.emit(EVENT_TYPES.MODE.CHANGE, mode, modeOptions);
  }
}

const createModel = function() {
  return new EditorModel();
};

export { EditorModel, createModel };

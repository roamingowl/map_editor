import debug from 'debug';

const log = debug('script-loader');

class Loader {
  constructor() {
    this.scripts = {};
  }

  /**
   *
   * @param src
   * @return {Promise<any>}
   * @private
   */
  _load(src) {
    return new Promise((resolve, reject) => {
      log(`Loading ${src}`);
      const script = document.createElement('script');
      script.onload = function() {
        log(`${src} successfully loaded`);
        return resolve();
      };
      script.onerror = function(oE) {
        log(`${src} load failed with error`, oE);
        return reject(new Error(`Script load ${oE.target.src} failed`));
      };
      script.src = src;

      document.head.appendChild(script);
    });
  }

  async ensureLoaded(src) {
    if (this.scripts[src]) {
      log(`${src} already loaded.`);
      return;
    }
    log(`${src} not yet loaded.`);
    await this._load(src);
    this.scripts[src] = true;
  }
}

export default new Loader();

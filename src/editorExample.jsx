import React from 'react';
import styled from 'styled-components';
import Grid from '@material-ui/core/es/Grid/Grid';
import TopMenu from './editorExample/topToolbar';
import { createModel } from './editorModel';
import StatusBar from './editorExample/statusbar';
import MapCanvas from './editorExample/mapCanvas';
import RightDrawer from './editorExample/rightDrawer';

const MainDiv = styled.div`
  position: relative;
  overflow: hidden;
`;

export default class EditorExample extends React.Component {
  constructor() {
    super();
    this.model = createModel();
  }

  componentDidMount() {
    console.log('height', this.props.height);
    // listen to model
  }

  render() {
    return (
      <MainDiv>
        <TopMenu editorModel={this.model} />
        <div style={{ height: this.props.height - 24 - 48 }}>
          <RightDrawer editorModel={this.model} height={this.props.height} />
          <MapCanvas
            editorModel={this.model}
            height={this.props.height - 24 - 48}
          />
        </div>
        <StatusBar editorModel={this.model} />
      </MainDiv>
    );
  }
}

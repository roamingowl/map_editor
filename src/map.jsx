import React from 'react';
import L from 'leaflet';
// import LDraw          from 'leaflet-draw/dist/leaflet.draw-src';
import LdrawDrag from 'leaflet-draw-drag';
import Dsadada from 'leaflet-path-transform';
import styled from 'styled-components';
import debug from 'debug';
import leafletCss from 'leaflet/dist/leaflet.css';
import leafletDrawCss from 'leaflet-draw/dist/leaflet.draw.css';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import LayersIcon from '@material-ui/icons/Layers';
import MoveIcon from '@material-ui/icons/ZoomOutMap';
import EditIcon from '@material-ui/icons/Create';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import AddIcon from '@material-ui/icons/Add';
import DrawIcon from '@material-ui/icons/Gesture';
import CheckIcon from '@material-ui/icons/Check';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Button from '@material-ui/core/Button';
import ReactJson from 'react-json-view';
import FlowerIcon from './plantIcon';
import Layer from './object/layer';
import EditorModel from './editorModel';

const uuid = require('uuid/v4');

const log = debug('map');

const MapDiv = styled.div`
  min-height: 500px;
  width: 100vw;
`;

const customMarker = L.Marker.extend({
  options: {
    data: null,
  },
});

const customPath = L.Polyline.extend({
  options: {
    data: null,
  },
});

const customPolygon = L.Polygon.extend({
  options: {
    data: null,
    // transform: true
  },
});

const customDrawPath = L.Draw.Polyline.extend({
  Poly: customPath,
});

const customDrawPolygon = L.Draw.Polygon.extend({
  Poly: customPolygon,
});

const customDrawMarker = L.Draw.Marker.extend({
  _createMarker(latlng) {
    return new customMarker(latlng, {
      icon: this.options.icon,
      zIndexOffset: this.options.zIndexOffset,
    });
  },
});

const baseLayer = {
  url: '//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
  options: {
    attribution:
      '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
    minZoom: 8,
    maxNativeZoom: 19,
    maxZoom: 25,
  },
};

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
});

export default withStyles(styles)(
  class extends React.Component {
    constructor(props) {
      super(props);
      this.id = uuid();
      this.map = null;
      this.layerMenuButton = null;
      this.layerCounter = 0;
      this.itemCounter = 0;
      this.movingItem = null;

      this.state = {
        selection: [],
        layersMenuOpened: false,
        drawMenuOpened: false,
        model: new EditorModel(),
        selectedLayer: null,
        mapClicksEnabled: true,
      };
    }

    openDrawMenu = evt => {
      this.setState({
        drawMenuButton: evt.currentTarget,
        drawMenuOpened: true,
      });
    };

    handleDrawMenuClose = evt => {
      this.setState({
        drawMenuOpened: false,
      });
    };

    openLayerMenu = evt => {
      this.setState({
        layerMenuButton: evt.currentTarget,
        layersMenuOpened: true,
      });
    };

    handleLayerMenuClose = evt => {
      this.setState({
        layersMenuOpened: false,
      });
    };

    async componentDidMount() {
      this.map = L.map(this.id).setView([37.75, -122.23], 10);
      this.map.on('click', evt => {
        //
        if (!this.state.mapClicksEnabled) {
          return;
        }
        console.log('evt-clk', evt);
        if (this.state.selection && this.state.selection.length > 0) {
          for (let i = 0; i < this.state.selection.length; i++) {
            console.log('Selection canceled', this.state.selection[i]);
            this.state.selection[i].disable();
            if (this.state.selection[i]._marker) {
              console.log('marker click enabled');
              const marker = this.state.selection[i]._marker;
              this.state.selection[i]._marker.once('click', evt =>
                this.onMarkerClicked(evt, { layer: marker }),
              );
            }
            if (this.state.selection[i]._poly) {
              console.log('path click enabled');
              const path = this.state.selection[i]._poly;
              this.state.selection[i]._poly.once('click', evt =>
                this.onPathClicked(evt, { layer: path }),
              );
            }
          }
          this.setState({
            selection: [],
          });
        }
      });
      this.map.on('draw:editmove', this.onMarkerMoved);
      this.map.on('draw:created', this.onDrawingCreated);
      // this.map.on('mousemove', (evt) => {
      //     if (!this.movingItem) {
      //         return;
      //     }
      //     //this.movingItem.startLatLng
      //     //console.log('moving diff: ', evt.latlng, this.movingItem.startLatlng);//evt.latLng.lat - this.movingItem.startLatLng.lng, evt.latLng.lat - this.movingItem.startLatLng.lng, );
      //     let diffLat = evt.latlng.lat - this.movingItem.startLatlng.lat;
      //     let diffLng = evt.latlng.lng - this.movingItem.startLatlng.lng;
      //     for (let i = 0; i < this.movingItem._latlngs.length; i++) {
      //         let group = this.movingItem._latlngs[i];
      //         for (let j = 0; j < group.length; j++) {
      //             let item = group[j];
      //             console.log('itm', item);
      //             // item.setLatLng(L.latLng(item.latlng.lat + diffLat, item.latlng.lng + diffLng));
      //             item.lat = item.lat + diffLat;
      //             item.lng = item.lng + diffLng;
      //         }
      //     }
      // });
      // window.addEventListener('mouseup', (evt) => {
      //     console.log('up!');
      //     if (!this.movingItem) {
      //         return;
      //     }
      //     this.movingItem = null;
      //     this.map.dragging.enable();
      //     //evt.originalEvent.stopPropagation();
      // })
      this.map.on('layeradd', evt => {
        // console.log('Layer added', evt);
      });
      this.map.on('layerremove', evt => {
        // console.log('Layer removed', evt);
      });
      L.tileLayer(baseLayer.url, baseLayer.options).addTo(this.map);
      log('Map loaded', this.map);
      this.selectLayer(this.addLayer());
    }

    onMarkerMoved = editedMarkerInstance => {
      console.log('edit moved!', editedMarkerInstance.layer);
      const originalData = editedMarkerInstance.layer.data;
      originalData.lat = editedMarkerInstance.layer.getLatLng().lat;
      originalData.lng = editedMarkerInstance.layer.getLatLng().lng;
      editedMarkerInstance.layer.data.layer.mapLayer.addLayer(
        editedMarkerInstance.layer,
      );
      this.setState({
        layers: this.state.layers,
        model: this.layersToModel(this.state.layers),
        editMode: false,
        transformMode: false,
      });
    };

    toggleEditMode = () => {
      this.setState({
        editMode: !this.state.editMode,
        transformMode: false,
      });
    };

    toggleTransferMode = () => {
      this.setState({
        transformMode: !this.state.transformMode,
        editMode: false,
      });
    };

    onPathClicked = (evt, markerInstance) => {
      this.setState({
        mapClicksEnabled: false,
      });
      setTimeout(() => {
        this.setState({
          mapClicksEnabled: true,
        });
      }, 200);
      console.log('edit path', evt, markerInstance.layer);
      const editPath = new L.Edit.Poly(markerInstance.layer);
      // editPath.initialize(markerInstance.layer);
      markerInstance.layer.options.editing = {};
      editPath.enable();
      this.state.selection.push(editPath);
      this.setState({
        selection: this.state.selection,
      });
    };

    onPolyClicked = (evt, markerInstance) => {
      if (!this.state.transformMode && !this.state.editMode) {
        return;
      }
      this.setState({
        mapClicksEnabled: false,
      });
      setTimeout(() => {
        this.setState({
          mapClicksEnabled: true,
        });
      }, 200);
      console.log('edit poly', evt, markerInstance.layer);

      if (this.state.transformMode) {
        console.log('transfer  mode on poly');
        markerInstance.layer.transform.enable({
          rotation: true,
          scaling: true,
          dragging: true,
        });
        this.state.selection.push(markerInstance.layer);
      }
      if (this.state.editMode) {
        console.log('edit mode on poly');
        markerInstance.layer.options.editing = {};
        const editPath = new L.Edit.Poly(markerInstance.layer);
        editPath.enable();
        this.state.selection.push(editPath);
      }
      this.setState({
        selection: this.state.selection,
      });
    };

    onMarkerClicked = (evt, markerInstance) => {
      const editMarker = new L.Edit.Marker(markerInstance.layer);
      // console.log(editMarker);
      // editMarker.initialize(markerInstance.layer);
      editMarker.enable();
      this.state.selection.push(editMarker);
      this.setState({
        selection: this.state.selection,
      });
    };

    onItemmove = evt => {
      console.log('move');
    };

    onDrawingCreated = markerInstance => {
      // TODO: currenlty only marker and polyline
      // TODO: finish polyline

      console.log('got drawing type ', markerInstance);
      if (markerInstance.layerType === 'marker') {
        const markerData = {
          name: 'Marker' + this.itemCounter,
          type: 'marker',
          id: uuid(),
          layerId: this.state.selectedLayer.id,
          layer: this.state.selectedLayer,
          lat: markerInstance.layer.getLatLng().lat,
          lng: markerInstance.layer.getLatLng().lng,
        };
        markerInstance.layer.data = markerData;
        markerInstance.layer.once('click', evt =>
          this.onMarkerClicked(evt, markerInstance),
        );
        markerInstance.layer.on('move', evt => {
          // TODO: if you want to multislect move - do it here!
          console.log('moved', evt);
        });
        this.state.selectedLayer.mapLayer.addLayer(markerInstance.layer);
        if (!this.state.selectedLayer.markers) {
          this.state.selectedLayer.markers = [];
        }
        this.state.selectedLayer.markers.push(markerData);
        this.itemCounter++;
        this.setState({
          // markersCounter: this.state.markersCounter + 1,
          layers: this.state.layers,
          model: this.layersToModel(this.state.layers),
        });
      } else if (markerInstance.layerType === 'polyline') {
        const lineData = {
          name: 'Path' + this.itemCounter,
          type: 'path',
          id: uuid(),
          layerId: this.state.selectedLayer.id,
          layer: this.state.selectedLayer,
          points: markerInstance.layer._latlngs,
        };
        markerInstance.layer.data = lineData;
        markerInstance.layer.once('click', evt =>
          this.onPathClicked(evt, markerInstance),
        );
        // markerInstance.layer.on('move', (evt) => {
        //     //TODO: if you want to multislect move - do it here!
        //     console.log('moved', evt);
        // });
        console.log('new path', markerInstance.layer);
        this.state.selectedLayer.mapLayer.addLayer(markerInstance.layer);
        if (!this.state.selectedLayer.paths) {
          this.state.selectedLayer.paths = [];
        }
        this.state.selectedLayer.paths.push(lineData);
        this.itemCounter++;
        this.setState({
          // pathsCounter: this.state.pathsCounter + 1,
          layers: this.state.layers,
          model: this.layersToModel(this.state.layers),
        });
      } else if (markerInstance.layerType === 'polygon') {
        const lineData = {
          name: 'Pply' + this.itemCounter,
          type: 'polygon',
          id: uuid(),
          layerId: this.state.selectedLayer.id,
          layer: this.state.selectedLayer,
          points: markerInstance.layer._latlngs,
        };
        markerInstance.layer.data = lineData;
        markerInstance.layer.once('click', evt =>
          this.onPolyClicked(evt, markerInstance),
        );
        // markerInstance.layer.once('mouseup', (evt) => this.onPolyClicked(evt, markerInstance));
        // markerInstance.layer.on('mousedown', (evt) => {
        //     console.log('down', evt);
        //     this.movingItem = markerInstance.layer;
        //     this.movingItem.startLatlng = evt.latlng;
        //     //this.map.on('mousemove', this.onItemmove);
        //     //disable map mousemove
        //     this.map.dragging.disable();
        // });
        // markerInstance.layer.on('mouseup', (evt) => {
        //     console.log('up');
        //     this.map.off('mousemove', this.onItemmove);
        //     this.map.dragging.enable();
        // });
        console.log('new poly', markerInstance.layer);
        this.state.selectedLayer.mapLayer.addLayer(markerInstance.layer);
        if (!this.state.selectedLayer.polygons) {
          this.state.selectedLayer.polygons = [];
        }
        this.state.selectedLayer.polygons.push(lineData);
        this.itemCounter++;
        this.setState({
          // pathsCounter: this.state.pathsCounter + 1,
          layers: this.state.layers,
          model: this.layersToModel(this.state.layers),
        });
      }
    };

    addPath = () => {
      this.setState({
        drawMenuOpened: false,
      });
      if (!this.state.selectedLayer) {
        return;
      }
      const line = new customDrawPath(this.map, {});
      line.enable();
    };

    addPoly = () => {
      this.setState({
        drawMenuOpened: false,
      });
      if (!this.state.selectedLayer) {
        return;
      }
      const poly = new customDrawPolygon(this.map, {
        shapeOptions: {
          transform: true,
          draggable: true,
        },
      });
      poly.enable();
    };

    addMarker = () => {
      this.setState({
        drawMenuOpened: false,
      });
      if (!this.state.selectedLayer) {
        return;
      }
      const size = [64, 64];
      const marker = new customDrawMarker(this.map, {
        icon: new FlowerIcon({
          iconSize: size,
          iconAnchor: [size[0] / 2, size[1]],
        }),
      });
      marker.enable();
    };

    addLayer = () => {
      // let layers    = this.state.layers;
      const drawLayer = new L.FeatureGroup();
      // let newLayer  = {
      //     name:     'Layer' + this.layerCounter,
      //     id:       uuid(),
      //     mapLayer: drawLayer
      // };
      const layer = new Layer({
        name: 'Layer' + this.layerCounter,
        mapLayer: drawLayer,
        map: this.map,
      });
      // layers.push(layer)
      this.state.model.addLayer(layer);
      this.map.addLayer(drawLayer);
      this.layerCounter++;
      this.setState({
        // layers,
        model: this.state.model, // this.layersToModel(layers),
        layersMenuOpened: false,
      });
      return layer;
    };

    layersToModel(layers) {
      const model = layers.map(layer => {
        const modelLayer = {};
        modelLayer.name = layer.name;
        modelLayer.id = layer.id;
        modelLayer.markers =
          layer.markers &&
          layer.markers.map(marker => {
            const modelMarker = {};
            modelMarker.name = marker.name;
            modelMarker.id = marker.id;
            modelMarker.type = marker.type;
            modelMarker.layerId = marker.layerId;
            modelMarker.lat = marker.lat;
            modelMarker.lng = marker.lng;
            return modelMarker;
          });
        if (!modelLayer.markers) {
          modelLayer.markers = [];
        }
        modelLayer.paths =
          layer.paths &&
          layer.paths.map(path => {
            const modelPath = {};
            modelPath.name = path.name;
            modelPath.id = path.id;
            modelPath.type = path.type;
            modelPath.layerId = path.layerId;
            modelPath.points = path.points;
            return modelPath;
          });
        if (!modelLayer.paths) {
          modelLayer.paths = [];
        }
        modelLayer.polygons =
          layer.polygons &&
          layer.polygons.map(path => {
            const modelPath = {};
            modelPath.name = path.name;
            modelPath.type = path.type;
            modelPath.id = path.id;
            modelPath.layerId = path.layerId;
            modelPath.points = path.points;
            return modelPath;
          });
        if (!modelLayer.polygons) {
          modelLayer.polygons = [];
        }
        return modelLayer;
      });
      return model;
    }

    render() {
      return (
        <div style={{ position: 'relative' }}>
          <AppBar position="static" color="default">
            <Toolbar variant="dense">
              <IconButton
                onClick={this.openLayerMenu}
                className={this.props.classes.menuButton}
                color="inherit"
                aria-label="Menu"
              >
                <LayersIcon />
              </IconButton>
              <Menu
                id="layers-menu"
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'right',
                }}
                anchorEl={this.state.layerMenuButton}
                open={this.state.layersMenuOpened}
                onClose={this.handleLayerMenuClose}
              >
                <MenuItem onClick={this.addLayer}>Add</MenuItem>
              </Menu>
              <IconButton
                onClick={this.openDrawMenu}
                className={this.props.classes.menuButton}
                color="inherit"
                aria-label="Draw"
              >
                <DrawIcon />
              </IconButton>
              <Menu
                id="draw-menu"
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'right',
                }}
                anchorEl={this.state.drawMenuButton}
                open={this.state.drawMenuOpened}
                onClose={this.handleDrawMenuClose}
              >
                <MenuItem onClick={this.addMarker}>Marker</MenuItem>
                <MenuItem onClick={this.addPath}>Path</MenuItem>
                <MenuItem onClick={this.addPoly}>Polygon</MenuItem>
              </Menu>
            </Toolbar>
          </AppBar>
          <Grid container spacing={24}>
            <Grid item style={{ width: 40 }}>
              <Grid container spacing={0}>
                <Grid item xs>
                  <IconButton
                    onClick={this.toggleTransferMode}
                    color={this.state.transformMode ? 'secondary' : ''}
                    className={this.props.classes.menuButton}
                    aria-label="Draw"
                  >
                    <MoveIcon />
                  </IconButton>
                </Grid>
                <Grid item style={{ width: 64 }}>
                  <IconButton
                    onClick={this.toggleEditMode}
                    color={this.state.editMode ? 'secondary' : ''}
                    className={this.props.classes.menuButton}
                    aria-label="Draw"
                  >
                    <EditIcon />
                  </IconButton>
                </Grid>
                <Grid item style={{ width: 64 }} />
              </Grid>
            </Grid>
            <Grid item xs>
              <MapDiv
                id={this.id}
                style={{
                  height: 500,
                  width: '100%',
                  position: 'relative',
                  overflow: 'hidden',
                }}
              />
            </Grid>
            <Grid item xs={3}>
              <Paper className={this.props.classes.root}>
                Layers
                <br />
                <List>
                  {this.state.layers.map((layer, idx) => (
                    <ListItem key={idx} onClick={() => this.selectLayer(layer)}>
                      {layer === this.state.selectedLayer && (
                        <ListItemIcon>
                          <CheckIcon />
                        </ListItemIcon>
                      )}
                      <ListItemText primary={layer.name} />
                    </ListItem>
                  ))}
                </List>
              </Paper>
            </Grid>
          </Grid>
          <br />
          <Grid container spacing={24}>
            <Grid item xs={12}>
              Selected layer:{' '}
              {this.state.selectedLayer
                ? this.state.selectedLayer.name
                : 'none'}{' '}
              | Selection: {this.getSelection()}
            </Grid>
          </Grid>
          <Grid container spacing={24}>
            <Grid item xs={12}>
              <ReactJson src={this.state.model.toJson()} />
            </Grid>
          </Grid>
        </div>
      );
    }

    getSelection = () => {
      if (this.state.selection.length === 0) {
        return 'nothing';
      }
      return `${this.state.selection.length} markers`;
    };

    selectLayer = layer => {
      this.setState({
        selectedLayer: layer,
      });
    };
  },
);

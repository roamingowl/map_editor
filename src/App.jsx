import React, { Component } from 'react';
import { hot } from 'react-hot-loader';
import './App.css';
import EditorExample from './editorExample';

class App extends Component {
  constructor(props) {
    super(props);
    this.fakeDiv = React.createRef();

    this.state = {
      height: null,
    };
  }

  componentDidMount() {
    this.setState({
      height: this.fakeDiv.current.clientHeight,
    });
  }

  render() {
    return (
      <div>
        <div ref={this.fakeDiv} style={{ width: '90vw', height: '90vh' }}>
          {this.state.height && <EditorExample height={this.state.height} />}
        </div>
      </div>
    );
  }
}

export default hot(module)(App);

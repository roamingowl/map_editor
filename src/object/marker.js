import L from 'leaflet';
import types from './types';
import BaseObject from './base';

export default class Marker extends BaseObject {
  constructor(options) {
    super();
    this.type = types.MARKER;
    this.map = options.map;
    this.meta = options.meta;
  }
}

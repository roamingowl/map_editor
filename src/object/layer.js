import types from './types';
import BaseObject from './base';
// import L          from 'leaflet';

export default class Layer extends BaseObject {
  constructor(options) {
    super();
    this.type = types.LAYER;
    this.map = options.map;
    this.mapLayer = options.mapLayer;
    this.meta = options.meta;
    this.children = [];
  }

  get name() {
    return this.meta.name;
  }

  add(obj) {
    this.children.push(obj);
  }

  toJson() {
    return this.children.map(child => child.toJson());
  }
}

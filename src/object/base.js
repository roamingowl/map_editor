import uuid from 'uuid/v4';
import types from './types';

export default class BaseObject {
  constructor() {
    this.id = uuid();
  }

  toJson() {
    throw new Error('not implemented');
  }
}
